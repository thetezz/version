package com.t3zz.version;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VersionTest {
	Version version;
	
	@Before
	public void setup(){
		try {
			version = new Version("1.1.1.1");
		} catch (VersionException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testValidateVersion() {
		assertTrue(Version.validateVersion("1.1.1.1"));
		assertFalse(Version.validateVersion("1.2.3.x"));
		assertFalse(Version.validateVersion("1.1.1.1.1"));
		assertFalse(Version.validateVersion("t3zz ownz"));
	}

	@Test
	public void testIncrementVersion() throws VersionException {
		version.incrementVersion(1);
		assertEquals("2.1.1.1", version.getVersion());
		version.incrementVersion(2);
		assertEquals("2.2.1.1", version.getVersion());
		version.incrementVersion(3);
		assertEquals("2.2.2.1", version.getVersion());
		version.incrementVersion(4);
		assertEquals("2.2.2.2", version.getVersion());
	}

	@Test
	public void testIsNum() {
		assertTrue(Version.isNum("69"));
		assertFalse(Version.isNum("asdf"));
	}
}
