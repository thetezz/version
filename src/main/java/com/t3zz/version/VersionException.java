package com.t3zz.version;

@SuppressWarnings("serial")
public class VersionException extends Exception {
    public VersionException(String message){
        super(message);    
    }
}
