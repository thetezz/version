package com.t3zz.version;

public class Version {
	private String version;
	
	public Version(String version) throws VersionException{
		if (validateVersion(version))
			this.version = version;
		else
			throw new VersionException("Invalid version string.");
	}
	
	public String getVersion(){
		return version;		
	}
	
	public void setVersion(String version){
		this.version = version;
	}
	
	//returns false if string version is composed of more than 4 integer
	//delimited by either '-' or '.'.
	public static Boolean validateVersion(String version) {		
		String [] tokens = version.split("-|\\.");
		if (tokens.length > 4) {
			return false;
		}
		
		for (int i = 0; i < tokens.length; i++) {
			if (! Version.isNum(tokens[i])) {
				return false; 
			}
		}
		return true;
	}
	
	public Boolean isGreaterThan(Version version){
		if(! validateVersion(version.getVersion())){
			return false;
		}
		if (! validateVersion(this.getVersion())){
			return false;
		}
		return true;
	}
	
	public void incrementVersion(int mode) throws VersionException {
		Character delimiter1 = null;
		Character delimiter2 =null;
		Character delimiter3 =null;
		String majorVersion = null;
		String minorVersion=null;
		String incrementalVersion=null;
		String buildNumber=null;
		
		if (version.length() > 0) {
			for (int i = 0; i < version.length(); i++) {
				if(version.charAt(i) == '.' || version.charAt(i)== '-') {
                      
					if (delimiter1==null){
						delimiter1 = version.charAt(i);
					    continue;
					}
					if (delimiter2==null) {
						delimiter2 = version.charAt(i);
						continue;
					}
					if (delimiter3==null){
						delimiter3 = version.charAt(i);
						continue;
					}		
				}
			}
		}
			
		String [] tokens = version.split("-|\\.");
		for (int i = 0; i < tokens.length; i++) {
			if (Version.isNum(tokens[i])) {
				if (i == 0)
					majorVersion = tokens[i];
				if (i == 1)
					minorVersion = tokens[i];
				if (i == 2 )
					incrementalVersion = tokens[i];
				if (i == 3)
					buildNumber = tokens[i];
			}
			else {
				throw new VersionException("Invalid version string.");
			}
		}
		
		if(mode ==1 && majorVersion != null){
			Integer x = Integer.valueOf(majorVersion);
			x++;
			majorVersion=(Integer.toString(x));
		}
		else if(mode == 2 && minorVersion != null){
			Integer x = Integer.valueOf(minorVersion);
			x++;
			minorVersion=(Integer.toString(x));
		}
		else if(mode == 3 && incrementalVersion != null){
			Integer x = Integer.valueOf(incrementalVersion);
			x++;
			incrementalVersion=(Integer.toString(x));
			
		}
		else if(mode == 4 && buildNumber!=null){
			Integer x = Integer.valueOf(buildNumber);
			x++;
			buildNumber=(Integer.toString(x));
		}
		else{
			throw new VersionException("mode - version mismatch");
		}
		if (delimiter3 != null)
			setVersion(majorVersion + delimiter1 + minorVersion + delimiter2 + 
					incrementalVersion + delimiter3 + buildNumber); 	
		else if (delimiter2 != null)
			setVersion(majorVersion + delimiter1 + minorVersion + delimiter2 + 
					incrementalVersion); 
		else if (delimiter1 != null)
			setVersion(majorVersion + delimiter1 + minorVersion); 
		else
			setVersion(majorVersion);
		
	/*
		if (delimiter1 != null)
			returnString = majorVersion + delimiter1 + minorVersion; 	
		else
			return majorVersion;
		if (delimiter2 != null)
			returnString = returnString + delimiter2 + incrementalVersion;
		else
			return returnString;
		if (delimiter3 != null)
			returnString = returnString + delimiter3 + buildNumber;
		return returnString;
		*/
				
	}	
	
	public static boolean isNum(String strNum) {
	    boolean ret = true;
	    try {
	        Double.parseDouble(strNum);
	    }catch (NumberFormatException e) {
	        ret = false;
	    }
	    return ret;
	}
}
